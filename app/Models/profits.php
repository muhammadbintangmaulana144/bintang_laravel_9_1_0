<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class profits extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable =[
        'transaction_id',
        'total'
    ];

    /**
     * transactions
     * 
     * @return void
     */
    public function transactions()
    {
        return $this->BelongTo(Transactions::class);
    }
}
